using System;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TransFortConsumer.DataBase;

[assembly: FunctionsStartup(typeof(TransFortConsumer.Startup))]
namespace TransFortConsumer
{
    class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            string SqlConnection = Environment.GetEnvironmentVariable("SqlConnectionString");
            string SqlConnectionStage = Environment.GetEnvironmentVariable("SqlConnectionStringStage");

            builder.Services.AddDbContext<StageDbContext>(
                            options => options.UseSqlServer(SqlConnectionStage));

            builder.Services.AddDbContext<TransFortDbContext>(
                options => options.UseSqlServer(SqlConnection));
        }
    }
}
